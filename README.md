# Carsharing

Сервис каршеринга.

**Ссылка на heroku:** https://itis-carsharing.herokuapp.com/cars

**Описание проекта:** сервис, в котором можно забронировать машину.

В сервисе представлены машины для аренды, каждая машина находится на определённой парковке, при возврате машины необходимо указать, на какой парковке вы её оставляете. Есть возможность поиска по маркам машин. Все арендованные машины и историю аренды можно посмотреть в разделе аренда. Сервис поддерживает разделение аккаунтов на 2 типа: пользователи и администрация. В сервисе присутствует возможность отправки сообщений от пользователя администратору и наоборот. Также реализован чат между пользователями на сокетах. 

**Стек технологий:** Gradle, Spring Boot (web, data, security, mail), Postgres, Liquibase, Hibernate, Lombok.

**Демонстрация работы сервиса по ссылке:**

[![Watch the video](https://i.ibb.co/jhP1wDv/Screenshot-4.png)](https://youtu.be/aiQE9jKbx6U)
