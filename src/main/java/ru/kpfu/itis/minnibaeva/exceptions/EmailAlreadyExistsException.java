package ru.kpfu.itis.minnibaeva.exceptions;

public class EmailAlreadyExistsException extends RuntimeException {

    public EmailAlreadyExistsException(String email) {
        super("user with email " + email + " already exists.");
    }

}
