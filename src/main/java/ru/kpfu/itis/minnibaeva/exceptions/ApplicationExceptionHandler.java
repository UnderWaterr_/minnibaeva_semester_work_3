package ru.kpfu.itis.minnibaeva.exceptions;

import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.security.AccountUserDetails;

@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(EmailAlreadyExistsException.class)
    public String handleEmailAlreadyExistsException() {
        return "redirect:/signIn?exists";
    }

    @ExceptionHandler(UserEmailNotFoundException.class)
    public String handleUserEmailNotFoundException(UserEmailNotFoundException exception, Model model, Authentication authentication) {
        return addExceptionOnPage(model, authentication, exception.getMessage());
    }

    @ExceptionHandler(EmptyMessageException.class)
    public String handleEmptyMessageException(EmptyMessageException exception, Model model, Authentication authentication) {
        return addExceptionOnPage(model, authentication, exception.getMessage());
    }

    private String addExceptionOnPage(Model model, Authentication authentication, String message) {
        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        if (user.getRole().toString().equals("ADMIN")) {
            model.addAttribute("isAdmin", true);
        } else {
            model.addAttribute("isAdmin", false);
        }

        model.addAttribute("error", message);
        model.addAttribute("canReply", true);

        return "message";
    }


}
