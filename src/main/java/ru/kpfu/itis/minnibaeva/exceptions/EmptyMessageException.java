package ru.kpfu.itis.minnibaeva.exceptions;

public class EmptyMessageException extends RuntimeException {
    public EmptyMessageException() {
        super("You can't send empty message.");
    }

}
