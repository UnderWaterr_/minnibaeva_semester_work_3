package ru.kpfu.itis.minnibaeva.exceptions;

public class UserEmailNotFoundException extends RuntimeException {

    public UserEmailNotFoundException(String clientEmail) {
        super("User with email " + clientEmail + " not found.");
    }

}
