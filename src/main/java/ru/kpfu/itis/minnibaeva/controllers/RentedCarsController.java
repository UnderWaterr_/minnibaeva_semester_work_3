package ru.kpfu.itis.minnibaeva.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.minnibaeva.dto.CarDto;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.security.AccountUserDetails;
import ru.kpfu.itis.minnibaeva.services.RentedCarsService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/rentedCars")
public class RentedCarsController {

    private final RentedCarsService rentedCarsService;

    @GetMapping
    public String getRentedCarsPage(Model model, Authentication authentication) {
        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        List<CarDto> cars = rentedCarsService.getAllWithClientId(user.getId());
        model.addAttribute("cars", cars);

        return "rentedCars";
    }

    @PostMapping
    public String returnCar(@RequestParam Integer carId, @RequestParam String location, Authentication authentication) {
        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        rentedCarsService.endTrip(carId, location, user.getId());

        return "redirect:/rentedCars";
    }

}
