package ru.kpfu.itis.minnibaeva.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.security.AccountUserDetails;
import ru.kpfu.itis.minnibaeva.services.ClientProfilePageService;

@Controller
@RequestMapping("/profile")
@RequiredArgsConstructor
public class ProfileController {

    private final ClientProfilePageService clientProfilePageService;

    @GetMapping
    public String getProfilePage(Authentication authentication, Model model) {
        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        model.addAttribute("client", clientProfilePageService.getClientById(user.getId()));

        return "profile";
    }
}
