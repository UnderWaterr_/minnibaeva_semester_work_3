package ru.kpfu.itis.minnibaeva.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.minnibaeva.dto.MailDto;
import ru.kpfu.itis.minnibaeva.exceptions.EmptyMessageException;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.security.AccountUserDetails;
import ru.kpfu.itis.minnibaeva.services.MailService;

@Controller
@RequestMapping("/message")
@RequiredArgsConstructor
public class MessageController {

    private final MailService mailService;

    @GetMapping
    public String getMail(@RequestParam(required = false) Integer messageId, Model model,
                          Authentication authentication) {

        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        model.addAttribute("canReply", true);


        if (messageId != null) {
            MailDto message = mailService.getMessage(messageId);

            model.addAttribute("message", message);

            if (message.getFrom().getEmail().equals(user.getEmail())) {
                model.addAttribute("canReply", false);
            }
        }


        if (user.getRole().toString().equals("ADMIN")) {
            model.addAttribute("isAdmin", true);
        } else {
            model.addAttribute("isAdmin", false);
        }

        return "message";
    }

    @PostMapping("/send")
    public String sendMail(@RequestParam(required = false) String clientEmail, @RequestParam String text, Authentication authentication) {
        if (text.equals("")) {
            throw new EmptyMessageException();
        }

        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        if (user.getRole().toString().equals("ADMIN")) {
            mailService.sendMail(user.getEmail(), clientEmail, text);
        } else {
            mailService.sendMail(user.getEmail(), "admin@mail.ru", text);
        }

        return "redirect:/message";
    }

}
