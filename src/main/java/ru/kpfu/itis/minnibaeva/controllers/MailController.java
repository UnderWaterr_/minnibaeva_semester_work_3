package ru.kpfu.itis.minnibaeva.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.minnibaeva.dto.MailDto;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.security.AccountUserDetails;
import ru.kpfu.itis.minnibaeva.services.MailService;

import java.util.List;

@Controller
@RequestMapping("/mail")
@RequiredArgsConstructor
public class MailController {

    private final MailService mailService;

    @GetMapping
    public String getMail(Model model, Authentication authentication) {
        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        List<MailDto> messages;

        if (user.getRole().toString().equals("ADMIN")) {
            model.addAttribute("isAdmin", true);
            messages = mailService.getAdminMessages();

        } else {
            model.addAttribute("isAdmin", false);
            messages = mailService.getClientMessages(user.getEmail());
        }


        model.addAttribute("messages", messages);

        return "mail";
    }
}
