package ru.kpfu.itis.minnibaeva.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainPageController {

    @GetMapping
    public String redirect() {
        return "redirect:/cars";
    }

    @GetMapping("/fine")
    public String getFinePage() {
        return "fine";
    }
}
