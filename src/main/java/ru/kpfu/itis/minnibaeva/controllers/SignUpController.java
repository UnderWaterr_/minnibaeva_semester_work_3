package ru.kpfu.itis.minnibaeva.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.minnibaeva.dto.SignUpForm;
import ru.kpfu.itis.minnibaeva.services.SignUpService;

import javax.validation.Valid;

@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @Autowired
    public SignUpController(SignUpService signUpService) {
        this.signUpService = signUpService;
    }

    @GetMapping
    public String getSignUpPage(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/";
        }

        return "signUp";
    }

    @PostMapping
    public String signUp(@Valid SignUpForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("signUpForm", form);
            return "signUp";
        }
        signUpService.signUp(form);
        return "redirect:/signIn?registered";
    }

    @GetMapping(value = "/confirmEmail/{confirm-code}")
    public String confirmEmail(Model model, @PathVariable("confirm-code") String confirmCode) {
        boolean result = signUpService.confirmClient(confirmCode);
        model.addAttribute("confirmResult", result);

        return "signIn";
    }
}
