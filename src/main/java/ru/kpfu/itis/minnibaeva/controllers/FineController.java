package ru.kpfu.itis.minnibaeva.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.minnibaeva.dto.ClientProfilePageDto;
import ru.kpfu.itis.minnibaeva.dto.FineDto;
import ru.kpfu.itis.minnibaeva.services.FineService;

import java.util.List;

@RestController
@RequestMapping("/fines")
@RequiredArgsConstructor
public class FineController {

    private final FineService fineService;

    @GetMapping
    public ResponseEntity<List<ClientProfilePageDto>> getFinePage() {
        return ResponseEntity.ok().body(fineService.getAllClients());
    }

    @PostMapping
    public void addFine(@RequestBody FineDto fine) {
        fineService.addFine(fine.getId());
    }

    @DeleteMapping
    public void deleteFine(@RequestBody FineDto fine) {
        fineService.deleteFine(fine.getId());
    }
}
