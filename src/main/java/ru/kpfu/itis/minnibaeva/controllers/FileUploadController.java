package ru.kpfu.itis.minnibaeva.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.security.AccountUserDetails;
import ru.kpfu.itis.minnibaeva.services.UploadFileService;

@Controller
@RequestMapping("/upload")
@RequiredArgsConstructor
public class FileUploadController {

    private final UploadFileService uploadFileService;

    @PostMapping
    public String uploadFile(@RequestParam("file") MultipartFile multipart, Authentication authentication) {
        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        uploadFileService.uploadFile(multipart, user.getId());

        return "redirect:/profile";
    }
}
