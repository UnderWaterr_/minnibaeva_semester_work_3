package ru.kpfu.itis.minnibaeva.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.security.AccountUserDetails;
import ru.kpfu.itis.minnibaeva.services.CarService;
import ru.kpfu.itis.minnibaeva.services.TripService;

import javax.servlet.http.HttpServlet;

@Controller
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarsController extends HttpServlet {

    private final CarService carService;
    private final TripService tripService;

    @GetMapping
    public String getCarsPage(Authentication authentication, Model model) {
        if (authentication != null) {
            User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();
            model.addAttribute("id", user.getId());
        }

        model.addAttribute("cars", carService.getAll());
        return "cars";
    }

    @PostMapping
    public String rentCar(Authentication authentication, @RequestParam Integer carId) {
        Integer id = ((AccountUserDetails) authentication.getPrincipal()).getUser().getId();
        tripService.save(id, carId);

        return "redirect:/cars";
    }


    @PostMapping("/search")
    public String searchCarByBrand(@RequestParam String brand, Authentication authentication, Model model) {
        if (authentication != null) {
            User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();
            model.addAttribute("id", user.getId());
        }

        model.addAttribute("cars", carService.getCarsByBrand(brand));
        return "cars";
    }

}
