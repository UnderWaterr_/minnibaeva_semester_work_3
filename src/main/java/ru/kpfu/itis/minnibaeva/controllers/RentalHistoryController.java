package ru.kpfu.itis.minnibaeva.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.minnibaeva.dto.TripDto;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.security.AccountUserDetails;
import ru.kpfu.itis.minnibaeva.services.RentalHistoryService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/history")
public class RentalHistoryController {

    private final RentalHistoryService rentalHistoryService;

    @GetMapping
    public String getHistory(Authentication authentication, Model model) {
        User user = ((AccountUserDetails) authentication.getPrincipal()).getUser();

        List<TripDto> trips = rentalHistoryService.getTripByUser(user.getId());
        model.addAttribute("trips", trips);

        return "rentalHistory";
    }
}
