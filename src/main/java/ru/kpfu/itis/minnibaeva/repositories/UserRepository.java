package ru.kpfu.itis.minnibaeva.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.minnibaeva.models.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);
}
