package ru.kpfu.itis.minnibaeva.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.kpfu.itis.minnibaeva.models.Trip;

import java.util.List;

public interface TripRepository extends JpaRepository<Trip, Integer> {

    @Query("select t from Trip t where t.car.id = ?1 and t.client.id = ?2 and t.endDate is null")
    Trip getTripForEnd(Integer carId, Integer clientId);

    List<Trip> getTripByClientIdOrderByStartDateDesc(Integer clientId);

    boolean existsByCarIdAndEndDateIsNull(Integer carId);

    boolean existsByCarIdAndClientIdAndEndDateIsNull(Integer carId, Integer clientId);
}
