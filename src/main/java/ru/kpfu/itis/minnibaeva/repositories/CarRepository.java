package ru.kpfu.itis.minnibaeva.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.kpfu.itis.minnibaeva.models.Car;

import java.util.List;

public interface CarRepository extends JpaRepository<Car, Integer> {
    @Query(value = "select * from car where car.id not in (select car_id from trip where end_date is null)", nativeQuery = true)
    List<Car> findAllFreeCars();

    @Query("select t.car from Trip t where t.client.id = ?1 and t.endDate is null")
    List<Car> getCarByClientId(Integer clientId);

    List<Car> findAllByBrandLikeIgnoreCase(String brand);
}
