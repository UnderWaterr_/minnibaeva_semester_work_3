package ru.kpfu.itis.minnibaeva.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.minnibaeva.models.Client;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Integer> {
    Client findByConfirmCode(String conformCode);

    List<Client> findAllByOrderByIdAsc();
}
