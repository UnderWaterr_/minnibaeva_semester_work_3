package ru.kpfu.itis.minnibaeva.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.minnibaeva.models.Mail;

import java.util.List;

public interface MailRepository extends JpaRepository<Mail, Integer> {
    List<Mail> getMailByFromUserEmailOrToUserEmailOrderByDateAsc(String fromUser, String toUser);
}
