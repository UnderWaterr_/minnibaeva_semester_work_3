package ru.kpfu.itis.minnibaeva.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.SignUpForm;
import ru.kpfu.itis.minnibaeva.exceptions.EmailAlreadyExistsException;
import ru.kpfu.itis.minnibaeva.models.Client;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.repositories.ClientRepository;
import ru.kpfu.itis.minnibaeva.services.SignUpService;
import ru.kpfu.itis.minnibaeva.util.EmailUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final ClientRepository clientRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailUtil emailUtil;

    @Value("${spring.mail.confirm_url}")
    private String confirmUrl;

    @Override
    public void signUp(SignUpForm form) {
        Client client = Client.builder()
                .firstName(form.getName())
                .lastName(form.getSurname())
                .email(form.getEmail())
                .password(passwordEncoder.encode(form.getPassword()))
                .driverLicense(form.getDriverLicense())
                .age(form.getAge())
                .rating(0D)
                .fine(0)
                .role(User.Role.USER)
                .state(User.State.NOT_CONFIRMED)
                .confirmCode(UUID.randomUUID().toString())
                .build();

        try {
            clientRepository.save(client);
        } catch (Exception e) {
            throw new EmailAlreadyExistsException(form.getEmail());
        }

        Map<String, String> data = new HashMap<>();
        data.put("confirmUrl", confirmUrl + client.getConfirmCode());
        data.put("firstName", form.getName());
        data.put("lastName", form.getSurname());

        emailUtil.sendMail(form.getEmail(), "Carsharing: Подтверждение почты", "confirm_email.ftlh",
                data);
    }

    @Override
    public boolean confirmClient(String confirmCode) {
        Client client = clientRepository.findByConfirmCode(confirmCode);

        if (client == null || client.getState().equals(User.State.CONFIRMED)) {
            return false;
        }

        client.setState(User.State.CONFIRMED);

        clientRepository.save(client);

        return true;
    }
}
