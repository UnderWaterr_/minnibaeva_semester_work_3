package ru.kpfu.itis.minnibaeva.services.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.minnibaeva.models.Client;
import ru.kpfu.itis.minnibaeva.repositories.ClientRepository;
import ru.kpfu.itis.minnibaeva.services.UploadFileService;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class UploadFileServiceImpl implements UploadFileService {

    private final Cloudinary cloudinary;
    private final ClientRepository clientRepository;

    @Override
    public void uploadFile(MultipartFile multipart, Integer userId) {
        try {
            String url = cloudinary.uploader().upload(multipart.getBytes(),
                    ObjectUtils.asMap("public_id", userId + "_avatar")).get("url").toString();

            Client client = clientRepository.getById(userId);
            client.setProfileImg(url);

            clientRepository.save(client);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
