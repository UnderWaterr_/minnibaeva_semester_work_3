package ru.kpfu.itis.minnibaeva.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.ClientProfilePageDto;
import ru.kpfu.itis.minnibaeva.models.Client;
import ru.kpfu.itis.minnibaeva.repositories.ClientRepository;
import ru.kpfu.itis.minnibaeva.services.FineService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FineServiceImpl implements FineService {

    private final ClientRepository clientRepository;

    @Override
    public List<ClientProfilePageDto> getAllClients() {
        return ClientProfilePageDto.from(clientRepository.findAllByOrderByIdAsc());
    }

    @Override
    public void addFine(Integer clientId) {
        Client client = clientRepository.getById(clientId);

        client.setFine(client.getFine() + 1);

        clientRepository.save(client);
    }

    @Override
    public void deleteFine(Integer clientId) {
        Client client = clientRepository.getById(clientId);

        if (client.getFine() > 0) {
            client.setFine(client.getFine() - 1);

            clientRepository.save(client);
        }
    }
}
