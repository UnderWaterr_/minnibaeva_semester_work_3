package ru.kpfu.itis.minnibaeva.services;

import ru.kpfu.itis.minnibaeva.dto.CarDto;

import java.util.List;

public interface CarService {
    List<CarDto> getAll();

    List<CarDto> getCarsByBrand(String brand);
}
