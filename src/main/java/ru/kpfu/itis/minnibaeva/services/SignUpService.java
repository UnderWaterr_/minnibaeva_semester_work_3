package ru.kpfu.itis.minnibaeva.services;

import ru.kpfu.itis.minnibaeva.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);

    boolean confirmClient(String confirmCode);
}
