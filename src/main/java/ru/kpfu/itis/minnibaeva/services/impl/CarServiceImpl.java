package ru.kpfu.itis.minnibaeva.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.CarDto;
import ru.kpfu.itis.minnibaeva.repositories.CarRepository;
import ru.kpfu.itis.minnibaeva.services.CarService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;

    @Override
    public List<CarDto> getAll() {
        return CarDto.from(carRepository.findAllFreeCars());
    }

    @Override
    public List<CarDto> getCarsByBrand(String brand) {
        return CarDto.from(carRepository.findAllByBrandLikeIgnoreCase("%" + brand.toLowerCase() + "%"));
    }
}
