package ru.kpfu.itis.minnibaeva.services;

import ru.kpfu.itis.minnibaeva.dto.MailDto;

import java.util.List;

public interface MailService {
    List<MailDto> getAdminMessages();

    List<MailDto> getClientMessages(String email);

    MailDto getMessage(Integer messageId);

    void sendMail(String email, String clientEmail, String text);
}
