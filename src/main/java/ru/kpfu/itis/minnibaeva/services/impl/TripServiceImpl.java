package ru.kpfu.itis.minnibaeva.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.models.Car;
import ru.kpfu.itis.minnibaeva.models.Client;
import ru.kpfu.itis.minnibaeva.models.Trip;
import ru.kpfu.itis.minnibaeva.repositories.TripRepository;
import ru.kpfu.itis.minnibaeva.services.TripService;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class TripServiceImpl implements TripService {

    private final TripRepository tripRepository;

    @Transactional
    @Override
    public void save(Integer id, Integer carId) {
        if (!tripRepository.existsByCarIdAndEndDateIsNull(carId)) {
            tripRepository.save(Trip.builder()
                    .client(Client.builder().id(id).build())
                    .car(Car.builder().id(carId).build())
                    .startDate(LocalDateTime.now())
                    .build());
        }
    }
}
