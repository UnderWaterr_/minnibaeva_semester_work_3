package ru.kpfu.itis.minnibaeva.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.MailDto;
import ru.kpfu.itis.minnibaeva.exceptions.UserEmailNotFoundException;
import ru.kpfu.itis.minnibaeva.models.Mail;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.repositories.MailRepository;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;
import ru.kpfu.itis.minnibaeva.services.MailService;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private final MailRepository mailRepository;
    private final UserRepository userRepository;

    @Override
    public List<MailDto> getAdminMessages() {
        return MailDto.from(mailRepository.findAll());
    }

    @Override
    public List<MailDto> getClientMessages(String email) {
        return MailDto.from(mailRepository.getMailByFromUserEmailOrToUserEmailOrderByDateAsc(email, email));
    }

    @Override
    public MailDto getMessage(Integer messageId) {
        return MailDto.from(mailRepository.getById(messageId));
    }

    @Override
    public void sendMail(String senderEmail, String clientEmail, String text) {

        User from = userRepository.findByEmail(senderEmail).get();

        Mail mail = Mail.builder()
                .date(LocalDateTime.now())
                .text(text)
                .fromUser(from)
                .build();

        User to;
        if (!clientEmail.equals("admin@mail.ru")) {
            to = userRepository.findByEmail(clientEmail).orElseThrow(() -> new UserEmailNotFoundException(clientEmail));
        } else {
            to = userRepository.findByEmail("admin@mail.ru").orElseThrow(() -> new UserEmailNotFoundException(clientEmail));
        }
        mail.setToUser(to);

        mailRepository.save(mail);

    }
}
