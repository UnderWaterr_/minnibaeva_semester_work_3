package ru.kpfu.itis.minnibaeva.services;

import ru.kpfu.itis.minnibaeva.dto.TripDto;

import java.util.List;

public interface RentalHistoryService {
    List<TripDto> getTripByUser(Integer clientId);
}
