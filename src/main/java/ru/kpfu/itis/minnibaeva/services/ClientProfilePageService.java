package ru.kpfu.itis.minnibaeva.services;

import ru.kpfu.itis.minnibaeva.dto.ClientProfilePageDto;

public interface ClientProfilePageService {
    ClientProfilePageDto getClientById(Integer id);
}
