package ru.kpfu.itis.minnibaeva.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.ClientProfilePageDto;
import ru.kpfu.itis.minnibaeva.repositories.ClientRepository;
import ru.kpfu.itis.minnibaeva.services.ClientProfilePageService;

@Service
@RequiredArgsConstructor
public class ClientProfilePageServiceImpl implements ClientProfilePageService {

    private final ClientRepository clientRepository;

    @Override
    public ClientProfilePageDto getClientById(Integer id) {
        return ClientProfilePageDto.from(clientRepository.getById(id));
    }
}
