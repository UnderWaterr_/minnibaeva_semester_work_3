package ru.kpfu.itis.minnibaeva.services;

import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {
    void uploadFile(MultipartFile multipart, Integer userId);
}
