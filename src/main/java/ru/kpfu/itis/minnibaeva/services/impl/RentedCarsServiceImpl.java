package ru.kpfu.itis.minnibaeva.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.CarDto;
import ru.kpfu.itis.minnibaeva.models.Car;
import ru.kpfu.itis.minnibaeva.models.Trip;
import ru.kpfu.itis.minnibaeva.repositories.CarRepository;
import ru.kpfu.itis.minnibaeva.repositories.TripRepository;
import ru.kpfu.itis.minnibaeva.services.RentedCarsService;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RentedCarsServiceImpl implements RentedCarsService {

    private final CarRepository carRepository;
    private final TripRepository tripRepository;

    @Override
    public List<CarDto> getAllWithClientId(Integer clientId) {
        return CarDto.from(carRepository.getCarByClientId(clientId));
    }

    @Override
    @Transactional
    public void endTrip(Integer carId, String location, Integer clientId) {
        if (tripRepository.existsByCarIdAndClientIdAndEndDateIsNull(carId, clientId)) {
            Car car = carRepository.getById(carId);
            car.setLocation(location);
            carRepository.save(car);

            Trip trip = tripRepository.getTripForEnd(carId, clientId);

            LocalDateTime endFate = LocalDateTime.now();

            trip.setEndDate(endFate);
            trip.setRentedTime(trip.getEndDate().toEpochSecond(ZoneOffset.UTC) - trip.getStartDate().toEpochSecond(ZoneOffset.UTC));
            trip.setPrice((int) (Math.ceil(trip.getRentedTime() / 60d) * car.getPrice()));

            tripRepository.save(trip);
        }
    }
}
