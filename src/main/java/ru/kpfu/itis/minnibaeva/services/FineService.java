package ru.kpfu.itis.minnibaeva.services;

import ru.kpfu.itis.minnibaeva.dto.ClientProfilePageDto;

import java.util.List;

public interface FineService {
    List<ClientProfilePageDto> getAllClients();

    void addFine(Integer clientId);

    void deleteFine(Integer clientId);
}
