package ru.kpfu.itis.minnibaeva.services;

import ru.kpfu.itis.minnibaeva.dto.CarDto;

import java.util.List;

public interface RentedCarsService {
    List<CarDto> getAllWithClientId(Integer clientId);

    void endTrip(Integer carId, String location, Integer clientId);
}
