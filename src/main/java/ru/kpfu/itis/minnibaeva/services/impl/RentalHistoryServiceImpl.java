package ru.kpfu.itis.minnibaeva.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.TripDto;
import ru.kpfu.itis.minnibaeva.repositories.TripRepository;
import ru.kpfu.itis.minnibaeva.services.RentalHistoryService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RentalHistoryServiceImpl implements RentalHistoryService {

    private final TripRepository tripRepository;

    @Override
    public List<TripDto> getTripByUser(Integer clientId) {
        return TripDto.from(tripRepository.getTripByClientIdOrderByStartDateDesc(clientId));
    }
}
