package ru.kpfu.itis.minnibaeva.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SignUpForm {
    private String name;
    private String surname;
    private String email;
    private String password;
    private long driverLicense;
    private int age;
}
