package ru.kpfu.itis.minnibaeva.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.minnibaeva.models.Mail;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class MailDto {
    private Integer id;
    private UserDto from;
    private UserDto to;
    private String text;
    private String date;

    public static MailDto from(Mail mail) {
        return MailDto.builder()
                .id(mail.getId())
                .from(UserDto.from(mail.getFromUser()))
                .to(UserDto.from(mail.getToUser()))
                .text(mail.getText())
                .date(mail.getDate().format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy")))
                .build();
    }

    public static List<MailDto> from(List<Mail> mail) {
        return mail.stream().map(MailDto::from).collect(Collectors.toList());
    }
}
