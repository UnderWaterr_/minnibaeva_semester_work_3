package ru.kpfu.itis.minnibaeva.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.minnibaeva.models.Trip;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TripDto {
    private Integer id;
    private CarDto car;
    private ClientProfilePageDto client;
    private Long rentedTime;
    private Integer price;
    private String startDate;
    private String endDate;

    public static TripDto from(Trip trip) {
        return TripDto.builder()
                .id(trip.getId())
                .car(CarDto.from(trip.getCar()))
                .client(ClientProfilePageDto.from(trip.getClient()))
                .rentedTime(trip.getRentedTime())
                .price(trip.getPrice())
                .startDate(trip.getStartDate().format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy")))
                .endDate(trip.getEndDate() == null ? null : trip.getEndDate().format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy")))
                .build();
    }

    public static List<TripDto> from(List<Trip> trips) {
        return trips.stream().map(TripDto::from).collect(Collectors.toList());
    }
}
