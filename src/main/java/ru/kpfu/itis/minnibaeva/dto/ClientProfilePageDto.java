package ru.kpfu.itis.minnibaeva.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.minnibaeva.models.Client;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ClientProfilePageDto {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private Long driverLicense;
    private Integer fine;
    private Integer age;
    private String photo;

    public static ClientProfilePageDto from(Client client) {
        return ClientProfilePageDto.builder()
                .id(client.getId())
                .age(client.getAge())
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .email(client.getEmail())
                .driverLicense(client.getDriverLicense())
                .photo(client.getProfileImg())
                .fine(client.getFine())
                .build();
    }

    public static List<ClientProfilePageDto> from(List<Client> clients) {
        return clients.stream().map(ClientProfilePageDto::from).collect(Collectors.toList());
    }
}
