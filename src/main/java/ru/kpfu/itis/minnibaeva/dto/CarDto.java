package ru.kpfu.itis.minnibaeva.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.minnibaeva.models.Car;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CarDto {
    private int id;

    private String brand;

    private String location;

    private double price;

    private String number;

    private String photo;

    public static CarDto from(Car car) {
        return CarDto.builder()
                .id(car.getId())
                .brand(car.getBrand())
                .location(car.getLocation())
                .price(car.getPrice())
                .number(car.getNumber())
                .photo(car.getPhoto())
                .build();
    }

    public static List<CarDto> from(List<Car> cars) {
        return cars.stream().map(CarDto::from).collect(Collectors.toList());
    }
}
