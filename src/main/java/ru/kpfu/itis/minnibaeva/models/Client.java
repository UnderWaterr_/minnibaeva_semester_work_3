package ru.kpfu.itis.minnibaeva.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@SuperBuilder
@Entity
public class Client extends User {
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number", unique = true)
    private String phoneNumber;

    @Column(name = "profile_img")
    private String profileImg;

    @Column(name = "driver_license")
    private Long driverLicense;

    private Integer age;

    private Double rating;

    private Integer fine;

    @Column(name = "confirm_code")
    private String confirmCode;
}
