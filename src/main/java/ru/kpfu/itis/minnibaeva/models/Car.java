package ru.kpfu.itis.minnibaeva.models;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@Entity
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String brand;

    private String location;

    private Double price;

    private String number;

    private String photo;

    @ManyToOne
    @JoinColumn(name = "parking_id")
    private Parking parking;
}
