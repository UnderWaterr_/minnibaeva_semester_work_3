package ru.kpfu.itis.minnibaeva.util;


import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class EmailUtil {
    private final JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String from;

    public void sendMail(String to, String subject, String templateName, Map<String, String> data) {

        Configuration configuration = prepareConfiguration();

        configuration.setClassForTemplateLoading(EmailUtil.class, "/templates");

        String string;
        try {
            Template template = configuration.getTemplate(templateName);

            StringWriter stringWriter = new StringWriter();
            template.process(prepareData(data), stringWriter);

            string = stringWriter.toString();

        } catch (IOException | TemplateException e) {
            throw new RuntimeException(e);
        }


        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setSubject(subject);
            messageHelper.setTo(to);
            messageHelper.setFrom(from);
            messageHelper.setText(string, true);
        };

        mailSender.send(preparator);

    }

    private Configuration prepareConfiguration() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_23);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        configuration.setLogTemplateExceptions(false);
        return configuration;
    }

    private Map<String, Object> prepareData(Map<String, String> userData) {
        Map<String, Object> data = new HashMap<>();
        data.put("userDetails", userData);
        return data;
    }
}
