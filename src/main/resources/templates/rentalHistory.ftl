<#ftl encoding='UTF-8'>

<#import "common.ftl" as navigationBar>
<#import "navigation.ftl" as navigation>

<@navigationBar.navigation pageName="История аренды">
    <@navigation.loggedUser/>

    <div class="mx-auto" style="width: 90%; margin-top: 20px;">

        <table class="table  table-dark">
            <thead>

            <tr>
                <th scope="col">Начало аренды</th>
                <th scope="col">Конец аренды</th>
                <th scope="col">Время аренды</th>
                <th scope="col">Цена аренды</th>
                <th scope="col">Бренд автомобиля</th>
                <th scope="col">Номер автомобиля</th>
            </tr>
            </thead>

            <#if trips??>
                <#list trips as trip>
                    <tr>
                        <td>${trip.startDate}</td>
                        <#if trip.endDate?has_content>
                            <td>${trip.endDate}</td>
                            <td>${(trip.rentedTime/60)?int} мин ${trip.rentedTime%60} сек</td>
                            <td>${trip.price} р</td>
                        <#else>
                            <td>В аренде</td>
                            <td>В аренде</td>
                            <td>В аренде</td>
                        </#if>
                        <td>${trip.car.brand}</td>
                        <td>${trip.car.number}</td>
                    </tr>
                </#list>
            </#if>

        </table>
    </div>


</@navigationBar.navigation>
