<#ftl encoding='UTF-8'>

<#import "common.ftl" as navigationBar>
<#import "navigation.ftl" as navigation>

<@navigationBar.navigation pageName="Почта ">
    <#if isAdmin == false>
        <@navigation.loggedUser/>
    <#else>

        <@navigation.admin/>
    </#if>


    <div class="mx-auto" style="width: 90%; margin-top: 20px;">

        <table class="table  table-dark">
            <thead>

            <tr>
                <th scope="col">Кому</th>
                <th scope="col">От кого</th>
                <th scope="col">Дата отправки</th>
                <th scope="col">Перейти к сообщению</th>
            </tr>
            </thead>

            <#if messages??>
                <#list messages as message>
                    <tr>
                        <td>${message.getTo().getEmail()}</td>
                        <td>${message.getFrom().getEmail()}</td>
                        <td>${message.getDate()}</td>
                        <td>
                            <form action="/message" method="get">
                                <input name="messageId" value="${message.id}" type="hidden">
                                <button type="submit" class="btn btn-secondary">открыть</button>
                            </form>
                        </td>
                    </tr>
                </#list>
            </#if>

        </table>
    </div>

</@navigationBar.navigation>
