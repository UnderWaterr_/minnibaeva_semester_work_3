<#macro registrationTemplate>
    <div class="mx-auto mt-3" style="width: 260px; color: white;">

        <form action="/signUp" method="post">
            Почта:

            <input type="text" name="email" autocomplete="off" minlength="6"
                   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" maxlength="50" aria-describedby="basic-addon1"
                   required class="form-control">

            <br>
            Пароль:
            <input type="password" name="password" autocomplete="off" id="inputPassword5" class="form-control"
                   aria-describedby="passwordHelpBlock" minlength="3" maxlength="50" required>
            <br>
            Имя:
            <input type="text" class="form-control" name="name" autocomplete="off" minlength="2" maxlength="30"
                   pattern="^[А-Я].*"
                   required>
            <br>
            Фамилия:
            <input type="text" class="form-control" name="surname" autocomplete="off" minlength="2" maxlength="30"
                   pattern="^[А-Я].*"
                   required>
            <br>
            Водительское удостоверение (10):
            <input type="text" class="form-control" name="driverLicense" autocomplete="off" pattern="[0-9]{10}"
                   required>
            <br>

            Возраст:
            <input type="number" class="form-control" name="age" autocomplete="off" min="18" max="99" required>
            <br>
            <input type="submit" class="btn btn-secondary" value="зарегистрироваться">
        </form>


    </div>

</#macro>