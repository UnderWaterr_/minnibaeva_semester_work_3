<#ftl encoding='UTF-8'>

<#import "common.ftl" as navigationBar>
<#import "navigation.ftl" as navigation>

<@navigationBar.navigation pageName="Автомобили">
    <#if id?has_content>
        <@navigation.loggedUser/>
    <#else>

        <@navigation.unloggedUser/>
    </#if>


    <form action="/cars/search" method="post">

        <div class="input-group" style="margin-left: 42px; margin-top: 15px; ">
            <button type="submit" class="btn btn-secondary" style="position: revert">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search"
                     viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>

            </button>
            <div class="form-outline">
                <input type="search" name="brand" id="form1" class="form-control" placeholder="Найти по бренду"/>
            </div>

        </div>
    </form>

    <div class="container-fluid col-xl-px-10 col-lg-px-5">
        <div class="row m-3">

            <#list cars as car>

                <div class="col  m-1">
                    <div class="card text-white bg-dark  h-100" style="width: 18rem; position: revert">
                        <img class="card-img-top" src="${car.photo}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">${car.brand}</h5>
                            <p class="card-text">Местонахождение: ${car.location}</p>
                            <p class="card-text">Цена: ${car.price} р/мин</p>
                            <p class="card-text">Номер машины: ${car.number}</p>


                            <#if id?has_content>
                            <form action="/cars" method="post">
                                <input type='hidden' name='carId' value='${car.id}'/>
                                <button type="submit" class="btn btn-secondary">арендовать</button>
                                <#else>

                                    <input type="submit" id="myBtn" class="btn btn-secondary" value="Бронировать"
                                           onclick="onClick()"/>
                                    <div id="myModal" class="modal">
                                        <div class="modal-content">
                                            <span class="close" onclick="onClick2()" style="color: black">&times;</span>
                                            <p style="color: black">Чтобы арендовать автомобиль необходимо войти в
                                                аккаунт.</p>
                                        </div>
                                    </div>
                                </#if>

                            </form>
                        </div>
                    </div>
                </div>
            </#list>
        </div>
    </div>



    <script>
        var modal = document.getElementById('myModal');
        var btn = document.getElementById('myBtn');
        var span = document.getElementsByClassName('close')[0];
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        function onClick() {
            modal.style.display = "block"
        }

        function onClick2() {
            modal.style.display = "none";
        }
    </script>
    <style>
        .m-1 {
            max-width: 370px;
        }

        .modal {
            display: none;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
        }

        .modal-content {
            background-color: #fefefe;
            margin: 15% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        .close {
            color: black;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }
    </style>

</@navigationBar.navigation>
