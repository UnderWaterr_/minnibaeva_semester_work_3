<#macro loggedUser>
    <div class="navbar_navBar" style="text-align: right;">
        <a href="/cars">Автомобили</a>

        <div class="dropdown_navBar">
            <button class="dropbtn_navBar" onclick="myFunction()">Аренда
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-content_navBar" id="myDropdown_navBar">
                <a href="/rentedCars">Арендованные автомобили</a>
                <a href="/history">История аренды</a>
            </div>
        </div>

        <a href="/profile">Профиль</a>
        <a href="/mail">Почта</a>
        <a href="/message">Написать сообщение</a>
        <a id="toggler" href="#">Написать в чат</a>
        <a href="/logout">Выйти</a>

        <div class="card" style="position: absolute">

            <#--сообщения-->
            <div id="box" style="display: none; background: white;">

                <div class="layer">
                    <table id="greetings">
                    </table>
                </div>

                <br>

                <div style="display: flex;">
                    <input type="text" id="message" class="form-control" placeholder="Your message..."
                           style="width: 200px; height: 35px;">

                    <button id="send" class="btn btn-default" onclick="sendMessage($('#message').val())">
                        Send
                    </button>
                </div>


            </div>
        </div>

        <style type="text/css">

            td {
                max-width: 250px;
                word-break: break-all;
            }

            .layer {
                text-align: left;
                overflow-y: scroll;
                width: 250px;
                height: 300px;
                padding: 5px;
                border: solid 1px black;
            }

            textarea {
                background: gray;
            }

            #box {
                position: fixed;
                right: 10px;
                bottom: 10px;
                padding: 10px;
            }

            textarea::placeholder {
                color: white;
            }

            label,
            textarea {
                font-size: .8rem;
                letter-spacing: 1px;
            }

            textarea {
                padding: 10px;
                max-width: 100%;
                line-height: 1.5;
                border-radius: 5px;
                border: 1px solid #ccc;
                box-shadow: 1px 1px 1px #999;
            }

            label {
                display: block;
                margin-bottom: 10px;
            }

        </style>

        <script type="text/javascript">
            window.onload = function () {
                document.getElementById('toggler').onclick = function () {
                    openbox('box', this);
                    return false;
                };
            };

            function openbox(id, toggler) {
                const div = document.getElementById(id);
                if (div.style.display === 'block') {
                    div.style.display = 'none';
                    toggler.innerHTML = 'Написать в чат';
                    disconnect();
                } else {
                    div.style.display = 'block';
                    toggler.innerHTML = 'Написать в чат';
                    connect();
                }
            }

            function myFunction() {
                document.getElementById("myDropdown_navBar").classList.toggle("show_navBar");
            }

            window.onclick = function (e) {
                if (!e.target.matches('.dropbtn_navBar')) {
                    const myDropdown = document.getElementById("myDropdown_navBar");
                    if (myDropdown.classList.contains('show_navBar')) {
                        myDropdown.classList.remove('show_navBar');
                    }
                }
            }
        </script>
        <script>
            function myFunction() {
                document.getElementById("myDropdown_navBar").classList.toggle("show_navBar");
            }

            window.onclick = function (e) {
                if (!e.target.matches('.dropbtn_navBar')) {
                    const myDropdown = document.getElementById("myDropdown_navBar");
                    if (myDropdown.classList.contains('show_navBar')) {
                        myDropdown.classList.remove('show_navBar');
                    }
                }
            }
        </script>

        <script type="text/javascript" src="/app.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>


    </div>
</#macro>

<#macro unloggedUser>
    <div class="navbar_navBar">
        <a href="/cars">Автомобили</a>
        <a href="/signIn">Войти</a>
        <a href="/signUp">Зарегистрироваться</a>

    </div>
</#macro>

<#macro admin>
    <div class="navbar_navBar">
        <a href="/fine">Добавить штрафы</a>
        <a href="/mail">Почта</a>
        <a href="/message">Написать сообщение</a>
        <a href="/logout">Выйти</a>
    </div>

    <style type="text/css">

        td {
            max-width: 250px;
            word-break: break-all;
        }

        .layer {
            text-align: left;
            overflow-y: scroll;
            width: 250px;
            height: 300px;
            padding: 5px;
            border: solid 1px black;
        }

        textarea {
            background: gray;
        }

        #box {
            position: fixed;
            right: 10px;
            bottom: 10px;
            padding: 10px;
        }

        textarea::placeholder {
            color: white;
        }

        label,
        textarea {
            font-size: .8rem;
            letter-spacing: 1px;
        }

        textarea {
            padding: 10px;
            max-width: 100%;
            line-height: 1.5;
            border-radius: 5px;
            border: 1px solid #ccc;
            box-shadow: 1px 1px 1px #999;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

    </style>

</#macro>