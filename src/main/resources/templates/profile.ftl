<#ftl encoding='UTF-8'>

<#import "common.ftl" as navigationBar>
<#import "navigation.ftl" as navigation>

<@navigationBar.navigation pageName="Профиль">
    <@navigation.loggedUser/>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');

        * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            font-family: 'Poppins', sans-serif;
            font-size: 14px
        }


        .breadcrumb a {
            text-decoration: none
        }

        .container {
            max-width: 1500px;
            padding: 0
        }

        p {
            margin: 0
        }

        .d-flex a {
            text-decoration: none;
            color: #686868
        }

        img {
            width: 100px;
            height: 100px;
            border-radius: 50%;
            object-fit: cover
        }

        #mainContainer {
            padding: 20px;
        }

        .upload {
            text-align: center;
        }
    </style>


    <div class="container" id="mainContainer">
        <div class="row">

            <div class="col-md-5">
                <div class="row">
                    <div class="col-12 bg-white p-0 px-3 py-3 mb-3">
                        <div class="d-flex flex-column align-items-center">

                            <#if client.photo?has_content>
                                <img class="photo" src="${client.photo}" alt="Фото профиля">
                            <#else>
                                <img class="photo" src="https://okeygeek.ru/wp-content/uploads/2020/03/no_avatar.png"
                                     alt="Фото профиля">

                            </#if>
                            <p class="fw-bold h4 mt-3">${client.firstName} </p>
                            <p class="text-muted">${client.lastName}</p>
                            <div class="d-flex text">
                                <div class="upload">
                                    <form action="/upload" method="post" enctype="multipart/form-data">
                                        <input type="file" name="file">
                                        <button type="submit" class="btn btn-primary">Сменить фото</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 ps-md-4">
                <div class="row">

                    <div class="col-12 bg-white px-3 mb-3 pb-3">
                        <div class="d-flex align-items-center justify-content-between border-bottom">
                            <p class="py-2">Почта</p>
                            <p class="py-2 text-muted">${client.email}</p>
                        </div>
                        <div class="d-flex align-items-center justify-content-between border-bottom">
                            <p class="py-2">Водительское удостоверение</p>
                            <p class="py-2 text-muted">${client.driverLicense}</p>
                        </div>
                        <div class="d-flex align-items-center justify-content-between border-bottom">
                            <p class="py-2">Возраст</p>
                            <p class="py-2 text-muted">${client.age}</p>
                        </div>
                        <div class="d-flex align-items-center justify-content-between border-bottom">
                            <p class="py-2">Штрафы</p>
                            <p class="py-2 text-muted">${client.fine}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@navigationBar.navigation>
