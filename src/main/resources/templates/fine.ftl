<#ftl encoding='UTF-8'>

<#import "common.ftl" as navigationBar>
<#import "navigation.ftl" as navigation>

<@navigationBar.navigation pageName="Добавление штрафов">
    <@navigation.admin/>
    <script>
        function getFines() {
            let request = new XMLHttpRequest();

            request.open('GET', '/fines', false);

            request.send();

            if (request.status !== 200) {
                alert('Ошибка!');
            } else {
                let html = '';

                let response = JSON.parse(request.response);

                for (let i = 0; i < response.length; i++) {
                    html += '<tr>';
                    html += '<td>' + response[i]['id'] + '</td>';
                    html += '<td>' + response[i]['email'] + '</td>';
                    html += '<td>' + response[i]['firstName'] + '</td>';
                    html += '<td>' + response[i]['lastName'] + '</td>';
                    html += '<td>' + response[i]['driverLicense'] + '</td>';
                    html += '<td>' + response[i]['fine'] + '</td>';
                    html += '<td>' + '<input type="submit" class="btn btn-secondary" value="добавить" onclick="addFine(' + response[i]['id'] + ')">' + '</td>';
                    if (response[i]['fine'] === 0) {
                        html += '<td> <input type="submit" class="btn btn-secondary" value="удалить" disabled </td>';
                    } else {
                        html += '<td> <input type="submit" class="btn btn-secondary" value="удалить" onclick="deleteFine(' + response[i]['id'] + ')"> </td>';
                    }
                    html += '</tr>';
                }

                document.getElementById('fines_table').innerHTML = html;
            }
        }

        function addFine(id) {
            let request = new XMLHttpRequest();

            let body = {
                "id": id
            };

            request.open('POST', '/fines', false);

            request.setRequestHeader('Content-Type', 'application/json');

            request.send(JSON.stringify(body));

            getFines();

        }

        function deleteFine(id) {
            let request = new XMLHttpRequest();

            let body = {
                "id": id
            };

            request.open('DELETE', '/fines', false);

            request.setRequestHeader('Content-Type', 'application/json');

            request.send(JSON.stringify(body));

            getFines();

        }

        window.onload = getFines;
    </script>
    <div class="mx-auto" style="width: 90%; margin-top: 20px;">

        <table class="table  table-dark">
            <thead>

            <tr>
                <th scope="col">Айди</th>
                <th scope="col">Почта</th>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">Водительское удостоверение</th>
                <th scope="col">Количество штрафов</th>
                <th scope="col">Добавить штраф</th>
                <th scope="col">Удалить штраф</th>
            </tr>
            </thead>
            <tbody id="fines_table">
            </tbody>
        </table>
    </div>
</@navigationBar.navigation>
