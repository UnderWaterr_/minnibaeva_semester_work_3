<#ftl encoding='UTF-8'>

<#macro navigation pageName>
    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" http-equiv="Content-Type">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF"
              crossorigin="anonymous">
        <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-kQtW33rZJAHjgefvhyyzcGF3C5TFyBQBA13V1RKPf4uH+bwyzQxZ6CmMZHmNBEfJ"
                crossorigin="anonymous"></script>
        <style>
            .navbar_navBar {
                overflow: hidden;
                background-color: #3b3a3d;
                font-family: Arial, Helvetica, sans-serif;
            }

            .navbar_navBar a {
                float: left;
                font-size: 16px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            .dropdown_navBar {
                float: left;
                overflow: hidden;
            }

            .dropdown_navBar .dropbtn_navBar {
                cursor: pointer;
                font-size: 16px;
                border: none;
                outline: none;
                color: white;
                padding: 14px 16px;
                background-color: inherit;
                font-family: inherit;
                margin: 0;
            }

            .navbar_navBar a:hover, .dropdown_navBar:hover .dropbtn_navBar, .dropbtn_navBar:focus {
                background-color: #6e6c78;
            }

            .dropdown-content_navBar {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
                z-index: 1;
            }

            .dropdown-content_navBar a {
                float: none;
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
                text-align: left;
            }

            .dropdown-content_navBar a:hover {
                background-color: #ddd;
            }

            .show_navBar {
                display: block;
            }

            html, body {
                background-color: #808080;

            }
        </style>
        <title>${pageName}</title>
    </head>
    <body>


    <#nested>

    </body>
    </html>
</#macro>
