<#macro loginTemplate servletName>

    <div class="mx-auto mt-3" style="width: 250px; color: white;">

        <form action="/${servletName}" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Почта</label>
                <input type="text" name="email" minlength="2" maxlength="50" class="form-control"
                       id="exampleInputEmail1" aria-describedby="emailHelp"
                       placeholder="Enter email" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Пароль</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                       placeholder="Password" minlength="2" maxlength="50" required>
            </div>
            <div class="form-check">
                <input type="checkbox" name="remember-me"/>
                <label class="form-check-label" for="remember-me">Запомнить меня</label>
            </div>
            <button type="submit" class="btn btn-secondary" name="submit" value="submit">войти</button>
        </form>
    </div>

</#macro>
