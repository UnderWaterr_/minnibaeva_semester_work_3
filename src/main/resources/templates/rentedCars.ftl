<#ftl encoding='UTF-8'>

<#import "common.ftl" as navigationBar>
<#import "navigation.ftl" as navigation>

<@navigationBar.navigation pageName="Арендованные автомобили">
    <@navigation.loggedUser/>
    <div class="container-fluid col-xl-px-10 col-lg-px-5">
        <div class="row m-3">

            <#list cars as car>

                <div class="col  m-1">
                    <div class="card text-white bg-dark  h-100" style="width: 18rem;">
                        <img class="card-img-top" src="${car.photo}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">${car.brand}</h5>
                            <p class="card-text">Местонахождение: ${car.location}</p>
                            <p class="card-text">Цена: ${car.price} р/мин</p>
                            <p class="card-text">Номер машины: ${car.number}</p>
                            <form action="/rentedCars" method="post">
                                <input type='hidden' name='carId' value='${car.id}'/>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="location" id="flexRadioDefault1"
                                           value="ул. Маршала Чуйкова, 62А" checked>
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        ул. Маршала Чуйкова, 62А
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="location" id="flexRadioDefault2"
                                           value="ул. Сибгата Хакима, 58" checked>
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        ул. Сибгата Хакима, 58
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="location" id="flexRadioDefault2"
                                           value="ул. Щапова, 37" checked>
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        ул. Щапова, 37
                                    </label>
                                </div>

                                <button type="submit" class="btn btn-secondary">вернуть</button>
                            </form>
                        </div>
                    </div>
                </div>
            </#list>
        </div>
    </div>

    <style>
        .m-1 {
            max-width: 370px;
        }
    </style>
</@navigationBar.navigation>
