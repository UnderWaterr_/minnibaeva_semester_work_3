<#ftl encoding='UTF-8'>

<#import "common.ftl" as navigationBar>
<#import "navigation.ftl" as navigation>


<@navigationBar.navigation pageName="Почта ">

    <#if isAdmin == true>
        <@navigation.admin/>
    <#else>
        <@navigation.loggedUser/>
    </#if>

    <#if error??>

        <div style="text-align: center; color:white; margin-top: 20px;">${error}</div>
    </#if>

    <#if message??>
        <div class="mx-auto" style="max-width: 550px; width: 60%; margin-top: 20px;">
            <div class="card">

                <div class="card-header">
                    Отправитель:
                    ${message.getFrom().getEmail()}
                    <br>
                    Дата отправки: ${message.getDate()}
                </div>
                <div class="card-body">
                    <div class="card-text">
                        ${message.getText()}
                    </div>
                </div>
            </div>
        </div>
    </#if>

    <#if canReply == true>

        <div class="mx-auto" style="max-width: 550px; width: 100%; margin-top: 20px;">
            <div class="card">
                <div class="card-text" style="width: 95%;  margin: 20px auto;">
                    <form action="/message/send" method="post">
                        <#if message??>
                            <input name='clientEmail' value='${message.getFrom().getEmail()}' readonly
                                   style="margin-bottom: 20px; margin-right: auto; width: 50%;"/>

                        <#else>

                            <#if isAdmin == true>
                                <input name='clientEmail' style="margin-bottom: 20px; margin-right: auto; width: 50%;"
                                       placeholder="Введите почту получателя"/>
                            <#else>
                                <input name='clientEmail' value='admin@mail.ru' readonly
                                       style="margin-bottom: 20px; margin-right: auto; width: 50%;"/>
                            </#if>
                        </#if>

                        <textarea rows="10" cols="90" name="text" id="txt" oninput="getTxt()"
                                  placeholder="Введите сообщение" maxlength="1000"></textarea>
                        <br>
                        <button type="submit" class="btn btn-secondary" id="sendMessageBtn">Отправить</button>

                    </form>
                    <div id="txt1">Всего символов: <span>0/1000</span></div>

                </div>
            </div>
        </div>


        <script>
            function getTxt() {
                var txt = document.getElementById('txt').value;
                txt_len = txt.length;

                document.getElementById('txt1').lastChild.innerText = txt_len + '/1000';
            }
        </script>


    </#if>


</@navigationBar.navigation>
