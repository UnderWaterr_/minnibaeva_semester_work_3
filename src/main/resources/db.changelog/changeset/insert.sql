insert into parking (location)
values ('ул. Маршала Чуйкова, 62А');
insert into parking (location)
values ('ул. Сибгата Хакима, 58');
insert into parking (location)
values ('ул. Щапова, 37');

insert into car (parking_id, brand, location, price, number, photo)
values (1, 'LADA Kalina', 'ул. Маршала Чуйкова, 62А', 5, 'В336ОК',
        'https://i.ibb.co/0Gd8X7B/sdcsdcsdcsdc.png');

insert into car (parking_id, brand, location, price, number, photo)
values (1, 'Renault Logan', 'ул. Маршала Чуйкова, 62А', 6, 'М529ДА',
        'https://i.ibb.co/YDdT5ph/sdcsdcsdc.png');

insert into car (parking_id, brand, location, price, number, photo)
values (1, 'Mazda 6', 'ул. Маршала Чуйкова, 62А', 8, 'Л419УР',
        'https://i.ibb.co/zSmHrYq/rtnwrtnwrtnwrtnwr.png');

insert into car (parking_id, brand, location, price, number, photo)
values (1, 'Mitsubishi ASX', 'ул. Маршала Чуйкова, 62А', 8, 'O068ЗИ',
        'https://i.ibb.co/Y7XnCdG/default-94406f1d1425ac7b6417e95bb70e06b2.png');

insert into car (parking_id, brand, location, price, number, photo)
values (1, 'BMW M', 'ул. Маршала Чуйкова, 62А', 7, 'П650ПХ',
        'https://i.ibb.co/k16xbmQ/gbwebaeb.png');

insert into car (parking_id, brand, location, price, number, photo)
values (1, 'BMW 7', 'ул. Маршала Чуйкова, 62А', 7, 'К892ИЕ',
        'https://i.ibb.co/Lpzrbsy/etbwrnyr-tu-5ryum.png');


INSERT INTO public.users (email, password, role, state)
VALUES ('minnibaeva.02@mail.ru', '$2a$10$08WODCWvddqz3aSl2/h2DOw1xFDBVQ4IU56xENXZI6hFmqUCSurV.', 'USER', 'CONFIRMED');
INSERT INTO public.client (age, confirm_code, driver_license, fine, first_name, last_name, phone_number, profile_img,
                           rating, id)
VALUES (18, '084c5412-19fa-4db4-8580-a7d00b177f37', 2342342444, 0, 'Алина', 'Миннибаева', null, null, 0, 1);


INSERT INTO public.users (email, password, role, state)
VALUES ('alina@mail.ru', '$2a$10$/oW3qxcA/H1fxZ3/6WpCTehg.JG/i8a0uLOmVUSjhbnIOsPsSODku', 'USER', 'CONFIRMED');
INSERT INTO public.client (age, confirm_code, driver_license, fine, first_name, last_name, phone_number, profile_img,
                           rating, id)
VALUES (18, '6c9f4b72-0676-424a-ac38-f4c51aa8a09d', 2342343525, 0, 'Алина', 'Миннибаева', null, null, 0, 2);



INSERT INTO public.users (email, password, role, state)
VALUES ('admin@mail.ru', '$2a$10$08WODCWvddqz3aSl2/h2DOw1xFDBVQ4IU56xENXZI6hFmqUCSurV.', 'ADMIN', 'CONFIRMED');


